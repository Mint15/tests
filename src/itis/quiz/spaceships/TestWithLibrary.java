package itis.quiz.spaceships;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class TestWithLibrary {
    CommandCenter commandCenter = new CommandCenter();
    static ArrayList<Spaceship> testingList = new ArrayList<>();

    @AfterEach
    void clearList(){
        testingList.clear();
    }

    @DisplayName("Выводит корабль с самой большой огневой мощью, отличной от нуля")
    @Test
    void getMostPowerfulShip_returnShipTest(){
        testingList.add(new Spaceship("First", 100, 0,0));
        testingList.add(new Spaceship("Second", 50, 0, 0));
        Spaceship result = commandCenter.getMostPowerfulShip(testingList);
        Assertions.assertEquals(result.getFirePower(), 100);
    }

    @DisplayName("Выводит самый первый по списку корабль с наибольшей огневой мощью")
    @Test
    void getMostPowerfulShip_returnFirstTest(){
        testingList.add(new Spaceship("First", 100, 0,0));
        testingList.add(new Spaceship("Second", 100, 0, 0));
        Spaceship result = commandCenter.getMostPowerfulShip(testingList);
        Assertions.assertEquals(result.getName(), "First");
    }

    @DisplayName("Выводит null, если кораблей с огневой мощью, отличной от нуля, нет")
    @Test
    void getMostPowerfulShip_NoArmedShipsTest(){
        testingList.add(new Spaceship("First", 0, 0,0));
        testingList.add(new Spaceship("Second", 0, 0, 0));
        Spaceship result = commandCenter.getMostPowerfulShip(testingList);
        Assertions.assertNull(result);
    }

    @DisplayName("Выводит корабль с искомым именем, когда такой корабль существует")
    @Test
    void getShipByName_shipFoundTest(){
        testingList.add(new Spaceship("First", 0, 0,0));
        testingList.add(new Spaceship("Second", 0, 0, 0));
        Spaceship result = commandCenter.getShipByName(testingList, "First");
        Assertions.assertEquals(result.getName(), "First");
    }

    @DisplayName("Выводит null, когда такой корабля с искомым именем не существует")
    @Test
    void getShipByName_shipNotFoundTest(){
        testingList.add(new Spaceship("First", 0, 0,0));
        testingList.add(new Spaceship("Second", 0, 0, 0));
        Spaceship result = commandCenter.getShipByName(testingList, "Third");
        Assertions.assertNull(result);
    }

    @DisplayName("Возвращает только корабли с достаточно большим грузовым трюмом для перевозки" +
            " груза заданного размера")
    @Test
    void getAllShipsWithEnoughCargoSpace_returnListTest(){
        testingList.add(new Spaceship("First", 0, 100,0));
        testingList.add(new Spaceship("Second", 0, 50, 0));
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testingList, 80);
        Assertions.assertTrue(result.get(0).getCargoSpace() >= 80);
    }

    @DisplayName("Возвращает пустой список, если кораблей с достаточно большим грузовым " +
            "трюмом для перевозки груза заданного размера нет")
    @Test
    void getAllShipsWithEnoughCargoSpace_returnEmptyListTest(){
        testingList.add(new Spaceship("First", 0, 100,0));
        testingList.add(new Spaceship("Second", 0, 50, 0));
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testingList, 200);
        Assertions.assertTrue(result.isEmpty());
    }

    @DisplayName("Возвращает только \"мирные\" корабли")
    @Test
    void getAllCivilianShips_returnListTest(){
        testingList.add(new Spaceship("First", 100, 100,0));
        testingList.add(new Spaceship("Second", 0, 50, 0));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testingList);
        Assertions.assertEquals(result.get(0).getFirePower(), 0);
    }

    @DisplayName("Возвращает пустой список, если \"мирных\" кораблей нет")
    @Test
    void getAllCivilianShips_returnEmptyListTest(){
        testingList.add(new Spaceship("First", 100, 100,0));
        testingList.add(new Spaceship("Second", 100, 50, 0));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testingList);
        Assertions.assertTrue(result.isEmpty());
    }
}