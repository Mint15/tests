package itis.quiz;


/*
	Это класс-представление рациональной дроби. Вам необходимо реализовать все методы приведенные ниже, для работы с ним.
	Вы можете добавлять собственные служебные внутренние методы, но не можете менять или удалять существующие.
	Для вашего удобства конструктор, геттеры и сеттеры уже есть.
	!МЕНЯТЬ СИГНАТУРЫ И ТИПЫ ВОЗВРАЩАЕМЫХ ЗНАЧЕНИЙ НЕЛЬЗЯ!
 */
public class RationalFraction {

	private int numerator;

	private int denominator;

	public RationalFraction(int numerator, int denominator) {
		this.numerator = numerator;
		this.denominator = denominator;
	}

	//сокращает дробь, насколько это возможно
	void reduce(){
		int gcd = 1;
		int absNumerator = Math.abs(numerator);
		int absDenominator = Math.abs(denominator);
		int minimum = Math.min(absNumerator, absDenominator);
		for (int i = 1; i <= minimum; i++) {
			if (absNumerator % i == 0 && absDenominator % i == 0 && i > gcd) {
				gcd = i;
			}
		}
		numerator /= gcd;
		denominator /= gcd;
	}
	//сравнивает с другой дробью (не забудьте про сокращение!)
	public boolean equals(RationalFraction otherFraction) {
		this.reduce();
		otherFraction.reduce();
		return numerator * otherFraction.getDenominator() == otherFraction.getNumerator() * denominator;
	}
	//Текстовое представление дроби
	@Override
	public String toString() {
		this.reduce();
		return (numerator + " / " + denominator);
	}
	//перевод в десятичную дробь
	public Double toDecimalFraction(){
		return (numerator / (double) denominator);
	}
	//выделение целой части
	public Integer getNumberPart(){
		return (numerator / denominator);
	}
	//сложение с другой дробью (не забудьте про сокращение здесь и во всех остальных математических операциях)
	public RationalFraction add(RationalFraction otherFraction){
		this.reduce();
		otherFraction.reduce();
		int genDenominator = denominator * otherFraction.getDenominator();
		int genNumerator = this.numerator * (genDenominator / denominator) + otherFraction.getNumerator() *
				(genDenominator / otherFraction.getDenominator());
		RationalFraction genFraction = new RationalFraction(genNumerator, genDenominator);
		genFraction.reduce();
		return genFraction;
	}

	//вычитание другой дроби
	public RationalFraction sub(RationalFraction otherFraction){
		this.reduce();
		otherFraction.reduce();
		int genDenominator = denominator * otherFraction.getDenominator();
		int genNumerator = this.numerator * (genDenominator / denominator) - otherFraction.getNumerator() *
				(genDenominator / otherFraction.getDenominator());
		RationalFraction genFraction = new RationalFraction(genNumerator, genDenominator);
		genFraction.reduce();
		return genFraction;
	}
	//умножение на другую дробь
	public RationalFraction multiply(RationalFraction otherFraction){
		this.reduce();
		otherFraction.reduce();
		int genDenominator = denominator * otherFraction.getDenominator();
		int genNumerator = numerator * otherFraction.getNumerator();
		RationalFraction genFraction = new RationalFraction(genNumerator, genDenominator);
		genFraction.reduce();
		return genFraction;
	}
	//деление на другую дробь
	public RationalFraction divide(RationalFraction otherFraction){
		this.reduce();
		otherFraction.reduce();
		int genDenominator = denominator * otherFraction.getNumerator();
		int genNumerator = numerator * otherFraction.getDenominator();
		RationalFraction genFraction = new RationalFraction(genNumerator, genDenominator);
		genFraction.reduce();
		return genFraction;
	}

	public int getNumerator() {
		return numerator;
	}

	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}

	public int getDenominator() {
		return denominator;
	}

	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}
}
